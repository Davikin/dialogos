using UnityEngine;

[System.Serializable]
public class AnchorPair
{
    public Vector2 min;
    public Vector2 max;

    public AnchorPair() {
        //Empty constructor
    }

    public AnchorPair(Vector2 _min, Vector2 _max) {
        min = _min;
        max = _max;
    }

    public Vector2 GetDelta() {
        return max - min;
    }

    public static AnchorPair GetHidingAnchorPair(AnchorPair _shownAnchorPair, Direction _dir) {
        AnchorPair result = new AnchorPair();
        Vector2 delta = _shownAnchorPair.GetDelta();

        switch (_dir) {
            case Direction.Up:
                result.min = new Vector2(_shownAnchorPair.min.x, 1f);
                result.max = new Vector2(_shownAnchorPair.max.x, 1f + delta.y);
                break;

            case Direction.Down:
                result.min = new Vector2(_shownAnchorPair.min.x, -delta.y);
                result.max = new Vector2(_shownAnchorPair.max.x, 0f);
                break;

            case Direction.Left:
                result.min = new Vector2(-delta.x, _shownAnchorPair.min.y);
                result.max = new Vector2(0, _shownAnchorPair.max.y);
                break;

            case Direction.Right:
                result.min = new Vector2(1f, _shownAnchorPair.min.y);
                result.max = new Vector2(1f + delta.x, _shownAnchorPair.max.y);
                break;
        }

        return result;
    }

    public AnchorPair AddAnchorPair(AnchorPair _addition) {
        this.max += _addition.max;
        this.min += _addition.min;
        return this;
    }

    public AnchorPair AddVector2(Vector2 _addition) {
        this.max += _addition;
        this.min += _addition;
        return this;
    }

    public AnchorPair AddAnchorPairRef(AnchorPair _addition) {
        AnchorPair result = new AnchorPair(this.min, this.max);

        result.max += _addition.max;
        result.min += _addition.min;

        return result;
    }

    public AnchorPair AddVector2Ref(Vector2 _addition) {
        AnchorPair result = new AnchorPair(this.min, this.max);

        result.max += _addition;
        result.min += _addition;

        return result;
    }
}

public enum Direction { Up, Down, Left, Right }