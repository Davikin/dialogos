
using UnityEngine;
using System;
using System.Collections.Generic;

public class DialogueBit
{
    public string id;
    public int index;
    public string text;
    public string es;

    public enum RowValues
    {
        NOTHING = 0,
        SOMETHING = 1
    }

    public static DialogueBit.RowValues GetRowValue(string name) {
        var values = (RowValues[])Enum.GetValues(typeof(RowValues));
        for (int i = 0; i < values.Length; i++) {
            if (values[i].ToString() == name) {
                return values[i];
            }
        }
        return values[0];
    }

    public string GetText() {
        return text;
    }
}