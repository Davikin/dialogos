using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDialogueData", menuName = "Dialogue/DialogueData")]
public class DialogueData : ScriptableObject {

    const string emptyTag = "<empty>";
    public CharacterDialogueProfile leftProfile, rightProfile;
    public enum Side { Left = 0, Right = 1 }

    [System.Serializable]
    public class Paragraph {
        public string name;

        public enum ParagraphType { Simple = 0, Question = 1}
        public ParagraphType paragraphType;

        [TextArea] public string simpleText;
        public Question decision;
        
        public Side side;

        CharacterDialogueProfile currentSideProfile;

        [Header("Sonido de expresion")]
        public bool playExpressionAudio;
        
        public int expressionIndex;

        //ClipId expressionClipId;

        [Header("Animacion de expresion")]
        public bool animate;
        public int animationIndex;

        [Header("Cambio de personaje")]
        public bool changeSpeaker;
        public CharacterDialogueProfile newSpeakerProfile;
        public Side newSpeakerSide;

        public string GetText() {
            string result = string.Empty;

            switch (paragraphType) {
                case ParagraphType.Simple:
                    result = simpleText;
                    break;

                case ParagraphType.Question:
                    result = decision.questionText;
                    break;
            }

            if (string.IsNullOrEmpty(result) || string.IsNullOrWhiteSpace(result)) {
                result = emptyTag;
            }

            return result;
        }

        public void HandleValidation(CharacterDialogueProfile _currentSideProfile) {
            currentSideProfile = _currentSideProfile;

            if (playExpressionAudio) {
                //expressionClipId = currentSideProfile.GetExpressionClipId(expressionIndex);
            }
        }
    }

    public Paragraph[] paragraphs;

    public UnityEngine.Events.UnityEvent OnEndDialogue;

    private void OnValidate() {
        ValidateParagraphs();
    }

    private void ValidateParagraphs() {
        CharacterDialogueProfile currentLeftProfile = leftProfile;
        CharacterDialogueProfile currentRightProfile = rightProfile;

        foreach (Paragraph p in paragraphs) {
            int endIndex = 19;

            if (p.GetText().Length < 20) {
                endIndex = p.GetText().Length - 1;
            }

            p.name = string.Empty;

            if (endIndex > 0) {
                p.name = p.GetText().Substring(0, endIndex).ToUpper().Replace(' ','_');
            }

            string sideString = "";
            switch (p.side) {
                case Side.Left:
                    sideString = "<-";
                    break;

                case Side.Right:
                    sideString = "->";
                    break;
            }

            p.name += " " + sideString;

            if (p.playExpressionAudio) {
                p.name += " (Au" + p.expressionIndex + ")";
            }

            if (p.animate) {
                p.name += " (An" + p.animationIndex + ")";
            }

            if (p.changeSpeaker) {
                p.name += " (CS)";
            }

            if (p.changeSpeaker && p.newSpeakerProfile != null) {
                switch (p.newSpeakerSide) {
                    case Side.Left:
                        currentLeftProfile = p.newSpeakerProfile;
                        break;

                    case Side.Right:
                        currentRightProfile = p.newSpeakerProfile;
                        break;
                }
            }

            switch (p.side) {
                case Side.Left:
                    p.HandleValidation(currentLeftProfile);
                    break;

                case Side.Right:
                    p.HandleValidation(currentRightProfile);
                    break;
            }
        }
    }

    [System.Serializable]
    public class Question {
        public string questionText = "�Ya editaste esta pregunta?";
        public List<Answer> possibleAnswers = new List<Answer>() {
            new Answer(){ answerText = "Yes"},
            new Answer(){ answerText = "No"}
        };
    }

    [System.Serializable]
    public class Answer {
        public string answerText;
        public UnityEngine.Events.UnityEvent answerEvent;
    }

    public void OpenSelf() {
        Dialogue.Instance.Open(this);
    }
}
