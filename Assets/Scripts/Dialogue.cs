using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using System.Collections.Generic;
using UnityEngine.InputSystem;

public class Dialogue : MonoBehaviour {
    public static Dialogue Instance { get; private set; }

    [SerializeField] private bool openOnStart = false;

    [Header("Datas")]
    [SerializeField] /**/
    //Solamente la asigne en el inspector para probar, en realidad esto provendra de cada NPC o Flow Controller que contenga una data
    private DialogueData data;

    [Header("Elementos del canvas")]
    [SerializeField] private TextMeshProUGUI emitterNameText;
    [SerializeField] private TextMeshProUGUI dialogueText;
    [SerializeField] private Image buttonSkip;
    private TextMeshProUGUI secondaryDialogueText;
    private static readonly Vector2 dialogueTextOffset = new Vector2(0, 0.06f);
    [SerializeField] private Image[] backBoxImages;
    private const float backBoxOffset = 0.1f;


    //TODO: DARLE UN ANCHOR PAIR DIFERENTE A CADA LAYER DE LA CAJA, CADA SHOWN ANCHOR SE OBTIENE AL INICIO
    //PODRIA PEDIRLE A COCO QUE HAGA SPRITES AL TAMAÑO NECESARIO, PERO SI LAS CAJAS SUFREN MAS CAMBIOS DESPUES, SERA MAS PEDO

    List<AnchorPair> layerHiddenAnchors = new List<AnchorPair>(), layerShownAnchors = new List<AnchorPair>();
    AnchorPair emitterBoxHiddenAnchors, emitterBoxShownAnchors;

    private static readonly Color clearWhite = Color.white - Color.black;
    [SerializeField] private Image emitterNameBoxImage, textBoxImage/*, scanlineImage*/, continueIconImage;
    //Vector3 continueIconInitialPos;
    AnchorPair continueIconInitialAnchors;
    [SerializeField] private Sprite nextParagraphSprite, endSpeechSprite;
    [SerializeField] private int textIndex = 0;

    [Header("Parametros de visualizacion")]
    [SerializeField] bool enterCharactersIndividually;
    [SerializeField] bool useSecondaryTextBox;
    [SerializeField] private float timeBetweenCharacters = 0.1f;
    private const float characterVolume = 0.5f;
    private enum ScrollType {
        None = 0, Up, Down, Left, Right, Grow, Shrink, Fade, Bounce
    }
    [SerializeField] ScrollType scrollType;
    private enum ToggleType {
        Appear = 0, Fill = 1, Scale = 2, TV = 3, Layers = 4
    }

    [SerializeField] ToggleType toggleType;

    [HideInInspector] public bool isOpen, isToggling;
    private Coroutine toggleBox_cr, writeText_cr, hoverContinueIcon_cr;
    private bool paragraphIsComplete = false;

    public static Action<bool> dialogueEvent;

    //private const float toggleDuration = 0.3f;
    private static float toggleDuration;

    [Serializable]
    private class Speaker {
        public RectTransform rectTransform;
        public Animator animator;
        [HideInInspector] public RuntimeAnimatorController prevAnimatorController;
        public Image image;
        public Image[] shadowImages;

        [HideInInspector] public AnchorPair shownAnchors, hiddenAnchors;
        [HideInInspector] public bool isEmitter;

        public void Toggle(bool _value) {
            image.gameObject.SetActive(_value);
        }

        public void SetPositions(DialogueData.Side _side) {
            rectTransform = image.GetComponent<RectTransform>();

            shownAnchors = new AnchorPair(rectTransform.anchorMin, rectTransform.anchorMax);

            switch (_side) {
                case DialogueData.Side.Left:
                    hiddenAnchors = new AnchorPair() {
                        min = new Vector2(-(shownAnchors.max.x - shownAnchors.min.x), shownAnchors.min.y),
                        max = new Vector2(0f, shownAnchors.max.y)
                    };
                    break;

                case DialogueData.Side.Right:
                    hiddenAnchors = new AnchorPair() {
                        min = new Vector2(1f, shownAnchors.min.y),
                        max = new Vector2(1f + (shownAnchors.max.x - shownAnchors.min.x), shownAnchors.max.y)
                    };
                    break;
            }

            rectTransform.anchorMin = hiddenAnchors.min;
            rectTransform.anchorMax = hiddenAnchors.max;
        }
    }

    [Header("Interlocutores")]
    [SerializeField] private Speaker leftSpeaker;
    [SerializeField] private Speaker rightSpeaker;
    private CharacterDialogueProfile currentLeftProfile, currentRightProfile;

    private static readonly Color currentSpeakerColor = Color.white;

    private Vector3 initialTextBoxPos;
    private AnchorPair initialTextBoxAnchors;
    private Color initialTextBoxColor;
    private bool transitioning = false;
    private static readonly Vector3 hiddenScale = new Vector3(0f, 0f, 1f);

    private AnchorPair frontBoxShownAnchors, frontBoxHiddenAnchors;

    [SerializeField] List<Color> shadowColors;
    [SerializeField] Vector2 shadowOffset;
    [SerializeField] CurveInfo toggleInCurveInfo, toggleOutCurveInfo, bounceInCurveInfo, bounceOutCurveInfo;

    [Header("Debugging")]
    [SerializeField] bool quickTest = false;
    [SerializeField] string quickTestString = "Lorem ipsum dolor sit amet";


    private void Awake() {
        Instance = this;

        //Movi estos dos comandos a Awake o las posiciones no se establecian a tiempo

        leftSpeaker.SetPositions(DialogueData.Side.Left);
        rightSpeaker.SetPositions(DialogueData.Side.Right);

#if !UNITY_EDITOR
        quickTest = false;
#endif
    }

    private void OnEnable() {
        //InputManager.Instance.ConfirmDialogueAction += ButtonPress;
        //InputManager.Instance.SkipDialogueAction += ButtonSkip;
    }

    private void OnDisable() {
        //InputManager.Instance.ConfirmDialogueAction -= ButtonPress;
        //InputManager.Instance.SkipDialogueAction -= ButtonSkip;

    }

    IEnumerator Start() {
        switch (toggleType) {
            case ToggleType.Appear:
                leftSpeaker.rectTransform.anchorMin = leftSpeaker.hiddenAnchors.min;
                leftSpeaker.rectTransform.anchorMax = leftSpeaker.hiddenAnchors.max;
                break;

            case ToggleType.Fill:
                emitterNameBoxImage.fillAmount = 0f;
                textBoxImage.fillAmount = 0f;
                //scanlineImage.fillAmount = 0f;
                break;

            case ToggleType.Scale:
            case ToggleType.TV:
                emitterNameBoxImage.transform.localScale = hiddenScale;
                textBoxImage.transform.localScale = hiddenScale;
                //scanlineImage.transform.localScale = hiddenScale;
                break;

            case ToggleType.Layers:
                for (int i = 0; i < backBoxImages.Length; i++) {
                    Image bbi = backBoxImages[i];

                    bbi.color = clearWhite;

                    AnchorPair newShownAnchors, newHiddenAnchors;

                    RectTransform rt = bbi.GetComponent<RectTransform>();
                    layerShownAnchors.Add(newShownAnchors = new AnchorPair(rt.anchorMin, rt.anchorMax));
                    layerHiddenAnchors.Add(newHiddenAnchors = new AnchorPair(newShownAnchors.min + Vector2.up * backBoxOffset,
                                                          newShownAnchors.max + Vector2.up * backBoxOffset));

                    rt.anchorMin = newHiddenAnchors.min;
                    rt.anchorMax = newHiddenAnchors.max;
                }

                RectTransform emitterBoxRT = emitterNameBoxImage.GetComponent<RectTransform>();
                emitterBoxShownAnchors = new AnchorPair(emitterBoxRT.anchorMin, emitterBoxRT.anchorMax);
                emitterBoxHiddenAnchors = new AnchorPair(emitterBoxShownAnchors.min + Vector2.up * backBoxOffset,
                                                         emitterBoxShownAnchors.max + Vector2.up * backBoxOffset);

                emitterBoxRT.anchorMin = emitterBoxHiddenAnchors.min;
                emitterBoxRT.anchorMax = emitterBoxHiddenAnchors.max;

                emitterNameBoxImage.color = clearWhite;

                emitterNameText.enabled = false;

                //Esto es para la caja frontal, la negra
                textBoxImage.color = clearWhite;
                RectTransform frontBoxRT = textBoxImage.GetComponent<RectTransform>();

                frontBoxShownAnchors = new AnchorPair(frontBoxRT.anchorMin, frontBoxRT.anchorMax);
                frontBoxHiddenAnchors = new AnchorPair(frontBoxShownAnchors.min + Vector2.up * backBoxOffset,
                                                       frontBoxShownAnchors.max + Vector2.up * backBoxOffset);

                textBoxImage.GetComponent<RectTransform>().anchorMin = frontBoxHiddenAnchors.min;
                textBoxImage.GetComponent<RectTransform>().anchorMax = frontBoxHiddenAnchors.max;

                dialogueText.enabled = false;
                break;
        }

        if (useSecondaryTextBox) {
            if (scrollType == ScrollType.Up || scrollType == ScrollType.Down ||
                scrollType == ScrollType.Left || scrollType == ScrollType.Right ||
                scrollType == ScrollType.Bounce) {
                secondaryDialogueText = Instantiate(dialogueText.gameObject, dialogueText.transform.parent).GetComponent<TextMeshProUGUI>();
                secondaryDialogueText.name = secondaryDialogueText.name.Replace("(Clone)", "_Secondary");
            }

            switch (scrollType) {
                case ScrollType.Up:
                    secondaryDialogueText.transform.localPosition += Vector3.down * dialogueTextOffset.y;
                    break;

                case ScrollType.Down:
                    secondaryDialogueText.transform.localPosition += Vector3.up * dialogueTextOffset.y;
                    break;
                case ScrollType.Left:
                    secondaryDialogueText.transform.localPosition += Vector3.right * dialogueTextOffset.x;
                    break;

                case ScrollType.Right:
                    secondaryDialogueText.transform.localPosition += Vector3.left * dialogueTextOffset.x;
                    break;

                case ScrollType.Bounce:
                    RectTransform secondaryDialogueTextRT = secondaryDialogueText.GetComponent<RectTransform>();
                    secondaryDialogueTextRT.anchorMin += Vector2.up * dialogueTextOffset.y;
                    secondaryDialogueTextRT.anchorMax += Vector2.up * dialogueTextOffset.y;
                    break;
            }
        }

        initialTextBoxPos = dialogueText.transform.localPosition;
        RectTransform dialogueTextRT = dialogueText.GetComponent<RectTransform>();
        initialTextBoxAnchors = new AnchorPair(dialogueTextRT.anchorMin, dialogueTextRT.anchorMax);
        initialTextBoxColor = dialogueText.color;

        toggleDuration = 0.3f; //TODO Review this time

        RectTransform continueIconRT = continueIconImage.GetComponent<RectTransform>();

        continueIconInitialAnchors = new AnchorPair(continueIconRT.anchorMin, continueIconRT.anchorMax);
        continueIconImage.enabled = false;

        if (openOnStart) {
            yield return new WaitForSeconds(1f);
            Open(data);
        } else {
            Close();
        }
    }

    private void SetSpeakerShadows(ref Speaker _speaker, CharacterDialogueProfile _profile) {
        if (_profile == null) {
            foreach (Image img in _speaker.shadowImages) {
                img.enabled = false;
                //print("Shadows were turned off because there is no profile set for this side");
            }
            return; //Este wey no existe, vamonos, no hay nada mas que hacer
        } else {
            foreach (Image img in _speaker.shadowImages) {
                img.enabled = true;
            }
        }

        int i = 0;

        foreach (Image shadowImg in _speaker.shadowImages) {
            shadowImg.sprite = _profile.GetShadowSprite();

            int colorIndex = i;
            //Tope de indices por si usamos mas de 2 sombras
            if (colorIndex > shadowColors.Count - 1) {
                colorIndex = shadowColors.Count - 1;
            }

            shadowImg.color = shadowColors[colorIndex];
            i++;
        }
    }

    private IEnumerator CheckIfSpeakerExpressionChanged(Speaker _speaker) {
        if (_speaker.prevAnimatorController != null && _speaker.animator.runtimeAnimatorController != null) {
            if (_speaker.prevAnimatorController != _speaker.animator.runtimeAnimatorController) {
                yield return StartCoroutine(ToggleShadows(_speaker, bounceOutCurveInfo));
                yield return StartCoroutine(ToggleShadows(_speaker, bounceInCurveInfo));
            }
        }

        _speaker.prevAnimatorController = _speaker.animator.runtimeAnimatorController;
    }

    private void SetSpeakers() {
        currentLeftProfile = data.leftProfile;
        currentRightProfile = data.rightProfile;

        SetSpeakerShadows(ref leftSpeaker, currentLeftProfile);

        if (currentLeftProfile != null) {
            //print("setting " + currentLeftProfile.GetCharacterName() + " to left profile");

            leftSpeaker.image.sprite = null;
            leftSpeaker.image.sprite = currentLeftProfile.GetExpressionSprite(0);

            //print("left profile should have " + currentLeftProfile.GetExpressionSprite(0).name + " as sprite");
            //print("but we have " + leftSpeaker.image.sprite.name+" the object's name is "+leftSpeaker.image.name);

            leftSpeaker.animator.runtimeAnimatorController = currentLeftProfile.GetExpressionAnimatorController(0);
            StartCoroutine(CheckIfSpeakerExpressionChanged(leftSpeaker));
            leftSpeaker.image.enabled = (leftSpeaker.image.sprite != null);
        } else {
            leftSpeaker.image.enabled = false;
            //print("Disabling left speaker image because it has no profile");
        }

        SetSpeakerShadows(ref rightSpeaker, currentRightProfile);
        if (currentRightProfile != null) {
            //print("setting " + currentLeftProfile.GetCharacterName() + " to left profile");

            rightSpeaker.image.sprite = null;
            rightSpeaker.image.sprite = currentRightProfile.GetExpressionSprite(0);

            //print("right profile should have " + currentRightProfile.GetExpressionSprite(0).name + " as sprite");
            //print("but we have " + rightSpeaker.image.sprite.name + " the object's name is " + rightSpeaker.image.name);

            rightSpeaker.animator.runtimeAnimatorController = currentRightProfile.GetExpressionAnimatorController(0);
            StartCoroutine(CheckIfSpeakerExpressionChanged(rightSpeaker));
            rightSpeaker.image.enabled = (rightSpeaker.image.sprite != null);
        } else {
            rightSpeaker.image.enabled = false;
            //print("Disabling right speaker image because it has no profile");
        }
    }

    public void ButtonPress(InputAction.CallbackContext ctx) {

        if (ctx.started) {
            if (isOpen && !isToggling && !transitioning) {
                if (paragraphIsComplete) {
                    if (textIndex < data.paragraphs.Length - 1) {
                        textIndex++;
                        WriteText();
                        //AudioManager.PlaySFX(ClipId.ContinueDialogue);
                        // AudioManager.PlaySFX(ClipId.CursorConfirm);
                    } else {
                        if (currentParagraph.paragraphType != DialogueData.Paragraph.ParagraphType.Question) {
                            Close();
                        }
                    }
                } else {
                    

                        CompleteParagraph();
                    
                }
            }
        }
    }

    private void ButtonSkip() {
        if (isOpen && !isToggling && !transitioning) {
            Close();
        }
    }

    private void Toggle(bool _value) {
        StopToggleBox();
        toggleBox_cr = StartCoroutine(Toggle_Coroutine(_value));
    }

    private IEnumerator Toggle_Coroutine(bool _value) {
        if (_value) {
            dialogueEvent?.Invoke(_value);

            StartCoroutine(ToggleSpeaker(leftSpeaker, _value));
            yield return StartCoroutine(ToggleSpeaker(rightSpeaker, _value));
            StartCoroutine(ToggleBox_Coroutine(_value));
        } else {
            yield return StartCoroutine(ToggleBox_Coroutine(_value));
            StartCoroutine(ToggleSpeaker(leftSpeaker, _value));
            StartCoroutine(ToggleSpeaker(rightSpeaker, _value));

            dialogueEvent?.Invoke(_value);
        }

        //buttonSkip.transform.parent.parent.gameObject.SetActive(_value);
    }

    private IEnumerator ToggleShadows(Speaker _speaker, CurveInfo _curveInfo) {
        List<RectTransform> shadowRts = new List<RectTransform>();

        foreach (Image img in _speaker.shadowImages) {
            shadowRts.Add(img.rectTransform);
        }

        foreach (RectTransform srt in shadowRts) {
            srt.gameObject.SetActive(_curveInfo.goesIn);
        }

        yield return null;
    }

    private IEnumerator ToggleSpeaker(Speaker _speaker, bool _value) {
        AnchorPair mainTargetAnchors = _value ? _speaker.shownAnchors : _speaker.hiddenAnchors;

        RectTransform mainRt = _speaker.rectTransform;
        mainRt.gameObject.SetActive(_value);

        mainRt.anchorMin = mainTargetAnchors.min;
        mainRt.anchorMax = mainTargetAnchors.max;

        if (!_value) {
            yield return StartCoroutine(ToggleShadows(_speaker, toggleOutCurveInfo)); //ToggleShadows espera un tween, por ende esta corrutina tambien lo hará
        }

        if (_value) {
            yield return StartCoroutine(ToggleShadows(_speaker, toggleInCurveInfo)); //ToggleShadows espera un tween, por ende esta corrutina tambien lo hará
        }
    }

    private IEnumerator ToggleBox_Coroutine(bool _value) {

        if (_value) {
            isOpen = _value;
        }
        isToggling = true;

        switch (toggleType) {
            case ToggleType.Appear:
                textBoxImage.gameObject.SetActive(_value);
                emitterNameBoxImage.gameObject.SetActive(_value);
                break;

            case ToggleType.Fill:
                yield return StartCoroutine(FillBox_Coroutine(_value));
                break;

            case ToggleType.Scale:
                yield return StartCoroutine(ScaleBox_Coroutine(_value));
                break;

            case ToggleType.TV:
                yield return StartCoroutine(ToggleTV_Coroutine(_value));
                break;

            case ToggleType.Layers:
                //yield return StartCoroutine(ToggleByLayers_Coroutine(_value));
                break;
        }

        isToggling = false;

        if (_value) {
            WriteText();
        } else {
            isOpen = _value;
        }
    }

    private IEnumerator FillBox_Coroutine(bool _value) {
        float targetValue = _value ? 1 : 0;
        float startTime = Time.time;
        float portionElapsed = 0;
        float initialTextBoxFill = textBoxImage.fillAmount;

        while (portionElapsed < 1f) {
            portionElapsed = (Time.time - startTime) / toggleDuration;
            textBoxImage.fillAmount = Mathf.Lerp(initialTextBoxFill, targetValue, portionElapsed);
            emitterNameBoxImage.fillAmount = Mathf.Lerp(initialTextBoxFill, targetValue, portionElapsed);
            //scanlineImage.fillAmount = Mathf.Lerp(initialTextBoxFill, targetValue, portionElapsed);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ScaleBox_Coroutine(bool _value) {
        Vector3 targetScale = _value ? Vector3.one : hiddenScale;

        //print("target scale: " + targetScale.ToString());

        float startTime = Time.time;
        float portionElapsed = 0;
        Vector3 initialTextBoxScale = textBoxImage.transform.localScale;

        while (portionElapsed < 1f) {
            portionElapsed = (Time.time - startTime) / toggleDuration;
            textBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            emitterNameBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            //scanlineImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ToggleTV_Coroutine(bool _value) {
        Vector3 targetScale = new Vector3(1f, 0.1f, 1f);

        //print("target scale: " + targetScale.ToString());

        float startTime = Time.time;
        float portionElapsed = 0;
        Vector3 initialTextBoxScale = textBoxImage.transform.localScale;
        float partialDuration = toggleDuration * 0.45f;

        while (portionElapsed < 1f) {
            portionElapsed = (Time.time - startTime) / partialDuration;
            textBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            emitterNameBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            //scanlineImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(toggleDuration * 0.1f);

        targetScale = _value ? Vector3.one : hiddenScale;
        startTime = Time.time;
        portionElapsed = 0;
        initialTextBoxScale = textBoxImage.transform.localScale;

        while (portionElapsed < 1f) {
            portionElapsed = (Time.time - startTime) / partialDuration;
            textBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            emitterNameBoxImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            //scanlineImage.transform.localScale = Vector3.Lerp(initialTextBoxScale, targetScale, portionElapsed);
            yield return new WaitForEndOfFrame();
        }
    }

    /*
    private IEnumerator ToggleByLayers_Coroutine(bool _value) {
        float timeBetweenLayers = 0.025f;

        if (_value) {
            for (int i = 0; i < backBoxImages.Length; i++) {
                yield return new WaitForSecondsRealtime(timeBetweenLayers); //Este wait es para que haya un pequeño desfase entre las layers
                StartCoroutine(ToggleLayer(backBoxImages[i], _value, layerShownAnchors[i].min, layerShownAnchors[i].max)); //Probablemente sea necesario asignar esa corrutina a una variable de corrutina
            }

            yield return new WaitForSecondsRealtime(timeBetweenLayers);
            StartCoroutine(ToggleLayer(textBoxImage, _value, frontBoxShownAnchors.min, frontBoxShownAnchors.max));
            yield return StartCoroutine(ToggleLayer(emitterNameBoxImage, _value, emitterBoxShownAnchors.min, emitterBoxShownAnchors.max));
        } else {
            // Lo mismo pero hacia atras
            StartCoroutine(ToggleLayer(emitterNameBoxImage, _value, emitterBoxHiddenAnchors.min, emitterBoxHiddenAnchors.max));
            StartCoroutine(ToggleLayer(textBoxImage, _value, frontBoxHiddenAnchors.min, frontBoxHiddenAnchors.max));
            yield return new WaitForSecondsRealtime(timeBetweenLayers);

            for (int i = backBoxImages.Length - 1; i >= 0; i--) {
                StartCoroutine(ToggleLayer(backBoxImages[i], _value, layerHiddenAnchors[i].min, layerHiddenAnchors[i].max));
                yield return new WaitForSecondsRealtime(timeBetweenLayers);
            }
        }
    }

    
    private IEnumerator ToggleLayer(Image _layerImg, bool _value, Vector2 _targetAnchorMin = default, Vector2 _targetAnchorMax = default) {
        float startTime = Time.time;
        float duration = 0.25f;
        RectTransform layerRT = _layerImg.GetComponent<RectTransform>();

        if (_value) {
            _layerImg.color = Color.white;
        }

        Vector2 targetAnchorMin = _targetAnchorMin;
        Vector2 targetAnchorMax = _targetAnchorMax;

        if (targetAnchorMin == Vector2.zero) {
            targetAnchorMin = _value ? layerShownAnchors[0].min : layerHiddenAnchors[0].min;
        }

        if (targetAnchorMax == Vector2.zero) {
            targetAnchorMax = _value ? layerShownAnchors[0].max : layerHiddenAnchors[0].max;
        }

        Tween minTween;
        sequence.Append(minTween = layerRT.DOAnchorMin(targetAnchorMin, duration).SetEase(Ease.InOutBack).SetUpdate(true));

        sequence.Append(layerRT.DOAnchorMax(targetAnchorMax, duration).SetEase(Ease.InOutBack).SetUpdate(true));
        yield return minTween.WaitForCompletion();

        if (!_value) {
            _layerImg.color = clearWhite;
        }
    }
    */

    private void StopToggleBox() {
        isToggling = false;
        if (toggleBox_cr != null) {
            StopCoroutine(toggleBox_cr);
        }
    }

    /// <summary>
    /// Abre el cuadro de dialogo con un dialogo compuesto.
    /// </summary>
    /// <param name="_data">Data del dialogo compuesto</param>
    public void Open(DialogueData _data) {
        //if(MusicManager.Instance != null)
        //    MusicManager.Instance.DialogueVolume(true);
        data = _data;

        print($"Data is {data.name}");

        SetSpeakers();
        SetCurrentEmitter(leftSpeaker);

        dialogueText.text = "";
        emitterNameText.text = "";
        emitterNameText.enabled = true;
        textIndex = 0;
        //buttonSkip.sprite = InputManager.Instance.buttonData.GetStartButton();


        Toggle(true);
        //AudioManager.PlaySFX(ClipId.OpenDialogue);
        //AudioManager.PlaySFX(ClipId.CursorConfirm);

        paragraphIsComplete = false;
    }

    public void Close() {
        StopWriteText(); //Para que la flecha no se quede flotando cuando demos skip

        //if (MusicManager.Instance != null)
        //    MusicManager.Instance.DialogueVolume(false);
        emitterNameText.enabled = false;
        StopHoverContinueIcon();
        continueIconImage.enabled = false;
        dialogueText.enabled = false;

        if (useSecondaryTextBox) {
            secondaryDialogueText.enabled = false;
        }

        Toggle(false);

        data.OnEndDialogue?.Invoke();
        //AudioManager.PlaySFX(ClipId.CloseDialogue);
    }



    private void WriteText() {
        writeText_cr = StartCoroutine(WriteText_Coroutine());
    }

    DialogueData.Paragraph currentParagraph = null;

    private IEnumerator WriteText_Coroutine() {
        foreach (var iab in instantiatedAnswerButtons) {
            iab.gameObject.SetActive(false);
        }

        currentParagraph = data.paragraphs[textIndex];

        if (currentParagraph.changeSpeaker) {
            StopHoverContinueIcon();
            yield return StartCoroutine(ChangeSpeaker_Coroutine(currentParagraph.newSpeakerSide, currentParagraph.newSpeakerProfile));
        }

        if (textIndex > 0) {

            switch (scrollType) {
                case ScrollType.Up:
                    yield return StartCoroutine(MoveTextBoxInSeconds(Vector3.up * dialogueTextOffset.y, 0.35f));
                    break;

                case ScrollType.Down:
                    yield return StartCoroutine(MoveTextBoxInSeconds(Vector3.down * dialogueTextOffset.y, 0.35f));
                    break;

                case ScrollType.Left:
                    yield return StartCoroutine(MoveTextBoxInSeconds(Vector3.left * dialogueTextOffset.x, 0.35f));
                    break;

                case ScrollType.Right:
                    yield return StartCoroutine(MoveTextBoxInSeconds(Vector3.right * dialogueTextOffset.x, 0.35f));
                    break;

                case ScrollType.Grow:
                    //yield return StartCoroutine(ScaleTextBoxInSeconds(Vector3.one * 50f, 0.35f));
                    break;

                case ScrollType.Shrink:
                    //yield return StartCoroutine(ScaleTextBoxInSeconds(Vector3.zero, 0.35f));
                    break;

                case ScrollType.Fade:
                    //yield return StartCoroutine(FadeTextBoxInSeconds(0.1f));
                    break;

                case ScrollType.Bounce:
                    //yield return StartCoroutine(BounceTextBoxInSeconds(0.3f));
                    break;
            }

            if (scrollType != ScrollType.None) {
                dialogueText.transform.localPosition = initialTextBoxPos;
                RectTransform dialogueTextRT = dialogueText.GetComponent<RectTransform>();
                dialogueTextRT.anchorMin = initialTextBoxAnchors.min;
                dialogueTextRT.anchorMax = initialTextBoxAnchors.max;
                dialogueText.transform.localScale = Vector3.one;
                dialogueText.color = initialTextBoxColor;
            }
        }

        dialogueText.text = "";
        dialogueText.enabled = true;

        if (useSecondaryTextBox) {
            secondaryDialogueText.enabled = true;
        }

        paragraphIsComplete = false;
        continueIconImage.enabled = false;

        switch (currentParagraph.side) {
            case DialogueData.Side.Left:
                SetCurrentEmitter(leftSpeaker);
                break;

            case DialogueData.Side.Right:
                SetCurrentEmitter(rightSpeaker);
                break;
        }

        // TODO Revisar porque esto hace play a mumbles incorrectos
        if (currentParagraph.playExpressionAudio) {
            switch (currentParagraph.side) {
                case DialogueData.Side.Left:
                    //AudioManager.PlaySFX(currentLeftProfile.GetExpressionClipId(currentParagraph.expressionIndex));
                    break;

                case DialogueData.Side.Right:
                    //AudioManager.PlaySFX(currentRightProfile.GetExpressionClipId(currentParagraph.expressionIndex));
                    break;
            }
        }

        if (currentParagraph.animate) {
            switch (currentParagraph.side) {
                case DialogueData.Side.Left:
                    leftSpeaker.animator.runtimeAnimatorController =
                    currentLeftProfile.GetExpressionAnimatorController(currentParagraph.animationIndex);
                    yield return StartCoroutine(CheckIfSpeakerExpressionChanged(leftSpeaker));
                    break;

                case DialogueData.Side.Right:
                    rightSpeaker.animator.runtimeAnimatorController =
                    currentRightProfile.GetExpressionAnimatorController(currentParagraph.animationIndex);
                    yield return StartCoroutine(CheckIfSpeakerExpressionChanged(rightSpeaker));
                    break;
            }
        }

        StopHoverContinueIcon();

        string textToDisplay = GetCurrentText();

        if (enterCharactersIndividually) {
            for (int i = 0; i < textToDisplay.Length; i++) {
                dialogueText.text += textToDisplay[i];
                if (textToDisplay[i] != ' ' && textToDisplay[i] != '\n') {
                    //AudioManager.PlaySFX(ClipId.CursorHub);
                }
                yield return new WaitForSecondsRealtime(timeBetweenCharacters);
            }
        }

        //AudioManager.PlaySFX(ClipId.EndParagrap);

        CompleteParagraph();
    }

    private IEnumerator MoveTextBoxInSeconds(Vector3 _offset, float _duration) {
        Vector3 originPos = dialogueText.transform.localPosition;
        Vector3 targetPos = originPos + _offset;

        float startTime = Time.time;
        float portionElapsed = 0;

        transitioning = true;

        while (portionElapsed <= 1f) {
            dialogueText.transform.localPosition = Vector3.Lerp(originPos, targetPos, portionElapsed);

            if (useSecondaryTextBox) {
                secondaryDialogueText.transform.localPosition = dialogueText.transform.localPosition - _offset;
            }
            portionElapsed = (Time.time - startTime) / _duration;
            yield return new WaitForEndOfFrame();
        }

        dialogueText.transform.localPosition = targetPos;

        if (useSecondaryTextBox) {
            secondaryDialogueText.transform.localPosition = initialTextBoxPos - _offset;
        }
        transitioning = false;
    }

    /*
    private IEnumerator BounceTextBoxInSeconds(float _duration) {
        RectTransform dialogueTextRT = dialogueText.GetComponent<RectTransform>();
        AnchorPair originAnchors = new AnchorPair(dialogueTextRT.anchorMin, dialogueTextRT.anchorMax);
        originAnchors.min += Vector2.up * dialogueTextOffset.y;
        originAnchors.max += Vector2.up * dialogueTextOffset.y;

        AnchorPair targetAnchors = new AnchorPair(dialogueTextRT.anchorMin, dialogueTextRT.anchorMax);

        dialogueTextRT.anchorMin = originAnchors.min;
        dialogueTextRT.anchorMax = originAnchors.max;

        Tween tween;
        float overshoot = 4f;
        transitioning = true;

        sequence.Append(
            tween = dialogueTextRT.DOAnchorMin(targetAnchors.min, _duration).SetEase(Ease.InOutBack, overshoot).SetUpdate(true)
        );

        sequence.Append(
            dialogueTextRT.DOAnchorMax(targetAnchors.max, _duration).SetEase(Ease.InOutBack, overshoot).SetUpdate(true)
        );

        yield return tween.WaitForCompletion();

        transitioning = false;
    }

    private IEnumerator ScaleTextBoxInSeconds(Vector3 _targetScale, float _duration) {
        Vector3 originScale = dialogueText.transform.localScale;

        float startTime = Time.time;
        float portionElapsed = 0;
        transitioning = true;

        while (portionElapsed <= 1f) {
            dialogueText.transform.localScale = Vector3.Lerp(originScale, _targetScale, portionElapsed);
            portionElapsed = (Time.time - startTime) / _duration;
            yield return new WaitForEndOfFrame();
        }

        dialogueText.transform.localScale = _targetScale;
        transitioning = false;
    }

    private IEnumerator FadeTextBoxInSeconds(float _duration) {
        Color originColor = dialogueText.color;
        Color targetColor = new Color(originColor.r, originColor.g, originColor.b, 0);

        float startTime = Time.time;
        float portionElapsed = 0;

        transitioning = true;

        while (portionElapsed <= 1f) {
            dialogueText.color = Color.Lerp(originColor, targetColor, portionElapsed);
            portionElapsed = (Time.time - startTime) / _duration;
            yield return new WaitForEndOfFrame();
        }

        dialogueText.color = targetColor;
        transitioning = false;
    }
    */

    private void StopWriteText() {
        if (writeText_cr != null) {
            StopCoroutine(writeText_cr);
        }
    }

    private string GetCurrentText() {
        if (quickTest) {
            return quickTestString;
        } else {
            try {
                return GetTextAtIndex(textIndex);
            } catch {
                throw new Exception("Array length: " + data.paragraphs.Length + ". Index requested: " + textIndex);
            }
        }
    }

    private string GetNextText() {
        try {
            int nextIndex = textIndex + 1;
            if (nextIndex > data.paragraphs.Length - 1) {
                return string.Empty;
            }

            return GetTextAtIndex(nextIndex);
        } catch {
            throw new Exception("Array length: " + data.paragraphs.Length + ". Index requested: " + textIndex);
        }
    }

    private string GetTextAtIndex(int _index) {
        return data.paragraphs[_index].GetText();
    }

    private void CompleteParagraph() {
        //print("complete paragraph");
        StopWriteText();
        dialogueText.text = GetCurrentText(); //Si lo hacemos aqui, el texto se setea despues del scroll
        if (useSecondaryTextBox) {
            secondaryDialogueText.text = GetNextText();
        }
        HoverContinueIcon();
        //AudioManager.PlaySFX(ClipId.EndParagrap);

        paragraphIsComplete = true;

        /*
        continueIconImage.sprite = null;
        if (textIndex < data.paragraphs.Length - 1) {
            continueIconImage.sprite = nextParagraphSprite;
        } else {
            continueIconImage.sprite = endSpeechSprite;
        }
        */

        if (instantiatedAnswerButtons.Count <= 0) {
            instantiatedAnswerButtons.Add(answerButton);
        }

        if (currentParagraph.paragraphType == DialogueData.Paragraph.ParagraphType.Question) {
            for (int i = 0; i < currentParagraph.decision.possibleAnswers.Count; i++) {
                if (instantiatedAnswerButtons.Count < i + 1) {
                    Button newButton = Instantiate(answerButton.gameObject, answerButton.transform.parent).GetComponent<Button>();
                    instantiatedAnswerButtons.Add(newButton);
                }
            }

            for (int i = 0; i < currentParagraph.decision.possibleAnswers.Count; i++) {
                instantiatedAnswerButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = currentParagraph.decision.possibleAnswers[i].answerText;

                int selectedAnswer = i;

                print($"Added listener OnSelectAnswer({selectedAnswer})");

                instantiatedAnswerButtons[i].onClick.RemoveAllListeners();
                instantiatedAnswerButtons[i].onClick.AddListener(() => OnSelectAnswer(selectedAnswer));
            }

            for (int i = 0; i < instantiatedAnswerButtons.Count; i++) {
                Button ab = instantiatedAnswerButtons[i];
                ab.gameObject.SetActive(true);
            }
        }
    }

    [SerializeField] Button answerButton;
    List<Button> instantiatedAnswerButtons = new List<Button>();

    private void OnSelectAnswer(int _answerIndex) {
        print($"currentParagraph is {currentParagraph.name}, chosen answer index: {_answerIndex}");
        currentParagraph.decision.possibleAnswers[_answerIndex].answerEvent?.Invoke();
    }

    private void HoverContinueIcon() {
        StopHoverContinueIcon();

        hoverContinueIcon_cr = StartCoroutine(HoverContinueIcon_Coroutine());
    }

    private IEnumerator HoverContinueIcon_Coroutine() {
        float x = 0;

        float hoverSpeed = 4f;
        //float hoverAmplitude = 6f; //Valor para posicion

        float hoverAmplitude = 0.025f;//Valor para anchors

        continueIconImage.enabled = true;
        float maxCircularValue = Mathf.PI;

        RectTransform continueIconRT = continueIconImage.GetComponent<RectTransform>();

        while (true) {
            if (x >= maxCircularValue) {
                x -= maxCircularValue;
            } else {
                x += hoverSpeed * Time.unscaledDeltaTime;
            }

            Vector2 variation = Vector3.up * Mathf.Sin(x) * hoverAmplitude;

            //continueIconImage.transform.localPosition = continueIconInitialPos + variation; //Para esto la variacion debe manejarse como Vector3
            continueIconRT.anchorMin = continueIconInitialAnchors.min + variation;
            continueIconRT.anchorMax = continueIconInitialAnchors.max + variation;

            yield return new WaitForEndOfFrame();
        }
    }
    

    private void StopHoverContinueIcon() {
        if (hoverContinueIcon_cr != null) {
            StopCoroutine(hoverContinueIcon_cr);
        }

        continueIconImage.enabled = false;
    }

    /*
    private void BlinkContinueIcon() {
        StopBlinkContinueIcon();

        blinkContinueIcon_cr = StartCoroutine(BlinkContinueIcon_Coroutine());
    }

    private IEnumerator BlinkContinueIcon_Coroutine() {
        while (true) {
            continueIconImage.enabled = !continueIconImage.enabled;
            yield return new WaitForSecondsRealtime(timeBetweenIconBlinks);
        }
    }
    */

    private void SetCurrentEmitter(Speaker _emitter) {
        Speaker receiver = _emitter == leftSpeaker ? rightSpeaker : leftSpeaker;
        CharacterDialogueProfile profileToSet = _emitter == leftSpeaker ? currentLeftProfile : currentRightProfile;

        emitterNameText.horizontalAlignment = _emitter == leftSpeaker ? HorizontalAlignmentOptions.Left : HorizontalAlignmentOptions.Right;

        if (profileToSet != null) {

            emitterNameText.text = profileToSet.GetCharacterName().ToUpper();

            _emitter.isEmitter = true;

            for (int i = 0; i < _emitter.shadowImages.Length; i++) {
                _emitter.shadowImages[i].transform.SetAsLastSibling();
            }
            _emitter.image.transform.SetAsLastSibling();

            StartCoroutine(ChangeImageColorTo(_emitter.image, currentSpeakerColor));
            //StartCoroutine(ChangeImageColorTo(_emitter.eyesImage, currentSpeakerColor));

            receiver.isEmitter = false;
            
            receiver.image.transform.SetAsFirstSibling();
            for (int i = receiver.shadowImages.Length - 1; i >= 0; i--) {
                receiver.shadowImages[i].transform.SetAsFirstSibling();
            }

            //StartCoroutine(ChangeImageColorTo(receiver.image, Colors.receiverGray));
            //StartCoroutine(ChangeImageColorTo(receiver.eyesImage, currentReceiverColor));

        }
    }

    private IEnumerator ChangeImageColorTo(Image _img, Color _targetColor) {
        //Color originColor = _img.color;
        //float duration = 0.5f;
        //float startTime = Time.time;
        //float portionElapsed = 0;

        //while (portionElapsed <= 1f) {
        //    //print("Color " + _targetColor);
        //    _img.color = Color.Lerp(originColor, _targetColor, portionElapsed);
        //    portionElapsed = (Time.time - startTime) / duration;
        //    yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        //}

        yield return null; // Para evitar error
    }

    private void SwapEmitters() {
        SetCurrentEmitter((leftSpeaker.isEmitter ? rightSpeaker : leftSpeaker));
    }

    private void ChangeSpeaker(DialogueData.Side _side, CharacterDialogueProfile _profile) {
        StartCoroutine(ChangeSpeaker_Coroutine(_side, _profile));
    }

    private IEnumerator ChangeSpeaker_Coroutine(DialogueData.Side _side, CharacterDialogueProfile _profile) {
        Speaker speakerToChange = null;
        transitioning = true;

        switch (_side) {
            case DialogueData.Side.Left:
                speakerToChange = leftSpeaker;
                currentLeftProfile = _profile;
                break;

            case DialogueData.Side.Right:
                speakerToChange = rightSpeaker;
                currentRightProfile = _profile;
                break;
        }

        yield return StartCoroutine(ToggleSpeaker(speakerToChange, false));

        try {
            if (_profile != null) {
                speakerToChange.image.enabled = true;
                speakerToChange.image.sprite = null;
                speakerToChange.image.sprite = _profile.GetExpressionSprite(0);

                //El animator debe establecerse tambien o se va a regresar a la imagen del   personaje anterior
                speakerToChange.animator.runtimeAnimatorController = _profile.GetExpressionAnimatorController(0);
                
                if (speakerToChange.image.sprite == null) {
                    speakerToChange.image.enabled = false;
                }
                //speakerToChange.eyesImage.enabled = true;
            } else {
                speakerToChange.image.enabled = false;
                //speakerToChange.eyesImage.enabled = false;
            }

            SetSpeakerShadows(ref speakerToChange, _profile); //Quiza mover esto fuera del if-else
        } catch {
            throw new Exception("No está bien asignado el perfil de personaje en "+data.name);
        }

        yield return StartCoroutine(CheckIfSpeakerExpressionChanged(speakerToChange));
        yield return StartCoroutine(ToggleSpeaker(speakerToChange, true));

        transitioning = false;
    }

    [System.Serializable]
    public class CurveInfo {
        public bool goesIn;
        public float duration = 0.2f;
        //public Ease easeType = Ease.InOutBack;
        public float overshoot;
    }
}