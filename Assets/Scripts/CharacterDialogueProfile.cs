using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// En este scriptable object concentraremos toda la información particular de un personaje
/// Su nombre, sprites, expresiones sonoras, corporales, animaciones, etc.
/// Esto lo hago de forma que no tengamos que cambiar los audios y sprites de un mismo personaje
/// a traves de cien archivos de conversacion, sino solo en uno.
/// </summary>

[CreateAssetMenu(fileName = "NewCharacterDialogueProfile", menuName = "Dialogue/CharacterDialogueProfile")]
public class CharacterDialogueProfile : ScriptableObject {

    [SerializeField] private string characterName = "NewCharacter";

    //[SerializeField] CharacterNames.RowValues localizedCharacterName;

    [SerializeField] private Sprite[] expressionSprites;
    [SerializeField] private Sprite shadowSprite;

    //[SerializeField] private ClipId[] expressionClipIds;

    [SerializeField] private RuntimeAnimatorController[] expressionAnimatorControllers;
    //public ClipId enterCharacterClipId;


#if UNITY_EDITOR
    private void OnValidate() {
        SetDefaultShadowSprite();
    }
#endif

    //En lo que creamos los verdaderos sprites para las sombras
    //Asigno automaticamente alguno de los que usemos para el retrato
    private void SetDefaultShadowSprite() {
        if (shadowSprite == null) {
            foreach (Sprite s in expressionSprites) {
                if (s != null) {
                    shadowSprite = GetExpressionSprite(0);
                }
                break;
            }
        }
    }

    public Sprite GetExpressionSprite(int _index) {
        
        if (expressionSprites != null && expressionSprites.Length > 0) {
            if (expressionSprites.Length - 1 >= _index) {
                if (expressionSprites[_index] != null) {
                    return expressionSprites[_index];
                }
            } else if (_index < 0) {
                return expressionSprites[0];
            } else {
                return expressionSprites[expressionSprites.Length - 1];
            }
        }

        //throw new System.Exception("No hay Sprites de expresion asignados a "+name);
        return null;
    }

    /*
    public ClipId GetExpressionClipId(int _index) {
        if (expressionClipIds != null && expressionClipIds.Length > 0) {
            if (expressionClipIds.Length - 1 >= _index) {
                if (expressionClipIds[_index] != ClipId.None) {
                    return expressionClipIds[_index];
                }
            } else if (_index < 0) {
                return expressionClipIds[0];
            } else {
                return expressionClipIds[expressionClipIds.Length - 1];
            }
        }

        throw new System.Exception("No hay AudioClipIds de expresion asignados a " + name);
    }
    */

    public RuntimeAnimatorController GetExpressionAnimatorController(int _index) {
        if (expressionAnimatorControllers != null && expressionAnimatorControllers.Length > 0) {
            if (expressionAnimatorControllers.Length - 1 >= _index) {
                if (expressionAnimatorControllers[_index] != null) {
                    return expressionAnimatorControllers[_index];
                }
            } else if (_index < 0) {
                return expressionAnimatorControllers[0];
            } else {
                return expressionAnimatorControllers[expressionAnimatorControllers.Length - 1];
            }
        }

        //throw new System.Exception("No hay RuntimeAnimatorControllers asignados a " + name);
        //Debug.LogError("No hay RuntimeAnimatorControllers asignados a " + name);
        //Debug.Log("No hay RuntimeAnimatorControllers asignados a " + name);

        return null;
    }

    public string GetCharacterName() {
        return characterName;
    }

    public Sprite GetShadowSprite() {
        return shadowSprite;
    }
}
